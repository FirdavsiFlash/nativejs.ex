/*
  TASK 1
    - Write a Person Constructor that initializes `name` and `age` from arguments.
    - All instances of Person should initialize with an empty `stomach` array.
    - Give instances of Person the ability to `.eat("someFood")`:
        + When eating an edible, it should be pushed into the `stomach`.
        + The `eat` method should have no effect if there are 10 items in the `stomach`.
    - Give instances of Person the ability to `.poop()`:
        + When an instance poops, its `stomach` should empty.
    - Give instances of Person a method `.toString()`:
        + It should return a string with `name` and `age`. Example: "Mary, 50"
*/


class People {
    constructor(options) {
        this.name = options.name
        this.age = options.age
        this.stomach = []
    }

    eat = function (food) {
        if (this.stomach.length <= 9) return this.stomach.push(food)
        else return console.log('cant eat anymore');
    }
    poop = function () {
        this.stomach.length = 0
    }

    toString = function () {
        return `${this.name}, ${this.age}`
    }
}

const David = new People({
    name: 'David',
    age: 50
})



/*
  TASK 2
    - Write a Car constructor that initializes `model` and `milesPerGallon` from arguments.
    - All instances built with Car:
        + should initialize with an `tank` at 0
        + should initialize with an `odometer` at 0
    - Give cars the ability to get fueled with a `.fill(gallons)` method. Add the gallons to `tank`.
    - STRETCH: Give cars ability to `.drive(distance)`. The distance driven:
        + Should cause the `odometer` to go up.
        + Should cause the the `tank` to go down taking `milesPerGallon` into account.
    - STRETCH: A car which runs out of `fuel` while driving can't drive any more distance:
        + The `drive` method should return a string "I ran out of fuel at x miles!" x being `odometer`.
*/


class Cars {
    constructor(options) {
        this.model = options.model
        this.milesPerGallon = options.milesPerGallon
        this.tank = 0
        this.odometer = 0
    }
    fill = function (gallons) {
        this.tank = this.tank + Number(gallons)
    }
    drive = function (distance) {
        if (this.tank >= this.milesPerGallon * distance) {
            this.odometer = this.odometer + distance
            this.tank = this.tank - (this.milesPerGallon * distance)

        } else {
            console.log(`I ran out of fuel at ${this.odometer + this.tank / this.milesPerGallon}`)
            this.fill(100)
        }
    }
}

const TeslaModelX = new Cars({
    model: 'Tesla Model 3',
    milesPerGallon: 0.8
})


/*
  TASK 3
    - Write a Baby constructor subclassing Person.
    - Besides `name` and `age`, Baby takes a third argument to initialize `favoriteToy`.
    - Besides the methods on Person.prototype, babies have the ability to `.play()`:
        + Should return a string "Playing with x", x being the favorite toy.
*/



class Person {
    constructor(options) {
        this.name = options.name
        this.age = options.age
    }

    info() {
        console.log(`Parent: ${this.name}`);
    }
}

class Baby extends Person {
    constructor(options) {
        super(options)
        this.favoriteToy = options.favoriteToy;
    }
    info() {
        super.info()
        console.log(`Baby ${this.name}`);
    }
    play() {
        console.log(`I'm playing with ${this.favoriteToy}`);
    }
}

const person1 = new Person({
    name: 'Fred',
    age: 35
})
const baby1 = new Baby({
    name: 'Suiii',
    age: 1,
    favoriteToy: 'ball'
})
console.log(person1);
baby1.info()

/* 
  TASK 4

  In your own words explain the four principles for the "this" keyword below:
  1. Window/ Global Object Binding
    -When in global scope, the 'this' keyword points to the window object. If a function is
    in the global scope, it is a method on the window object so that is where 'this' points.

  2. Implicit Binding
    - When a function is called with dot notation, the object before the dot becomes the 'this' keyword.

  3. New Binding
    - The 'this' keyword in a function constructor points to the window object.
    When creating a new object from a function constructor, the 'new' keyword creates a new empty object
    and the 'this' keyword now points to that new object.

  4. Explicit Binding
    - We can explicitly set what the 'this' keyword points to by using .call() or .apply()


*/



//Promises


const p1 = new Promise((resolve, reject) => {
    setTimeout(() => {
        console.log('time 1000');
    }, 1000);
    resolve('done')
}).then((req) => {
    setTimeout(() => {
        console.log('time 2000' + req);
    }, 2000);
})

// Exersizes for Promise(then, catch)


// The function job must return a promise object (you are in a NodeJS environment, you can use new Promise)
// The promise must resolve itself 2 seconds after the call to job and must provide hello world in the data
function job() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve('hello world');
        }, 2000);
    });
}

// module.exports = job;


// callback1 must be called only one time, after 2 seconds.
// callback2 must be called three times with an interval of 1 second.

function job(callback1, callback2) {

    let counter = 0
    setTimeout(() => {
        callback1()

    }, 2000)
    let call2 = setInterval(() => {
        if (counter == 3) {
            clearInterval(call2)
        } else {
            counter++
            callback2()
        }
    }, 1000);


}


// Your function must always return a promise
// If data is not a number, return a promise rejected instantly and give the data "error" (in a string)
// If data is an odd number, return a promise resolved 1 second later and give the data "odd" (in a string)
// If data is an even number, return a promise rejected 2 seconds later and give the data "even" (in a string)

function job(data) {
    return new Promise((resolve, reject) => {
        if (isNaN(data)) {
            reject('error');
        } else if (data % 2 === 0) {
            setTimeout(reject, 2000, 'even');
        } else {
            setTimeout(resolve, 1000, 'odd');
        }

    })
}



// Line 1: We call job1 and we store the returned promise in a variable called promise.
// Line 5: We call then on this promise and we attach a callback for when the promise is resolved
// Line 6: We print data1 and it is obvioulsy result of job 1 (see line 22)
// Line 7: On this line, we call job2 and we return the resulting promise. Keep that in mind and go to line 10.
// Line 10: We call then on the result of the first then. The result of then is always a promise. Always. At worst, it can be a never resolved promise, but it is a promise. In this case, the promise is the return value of job2 (called at line 7). When you are in a then callback, if you return a promise, it will be the resulting promise of the then call.
// Line 11: We print data2. According to the resolve call in the promise returned by job2 (called at line 7), data2 is result of job 2 (see line 30). By chaining our 2 promises (job1 then job2), job2 is always executed after job1. Line 6 is executed when the job1 promise is resolved, line 11 is executed when the job2 promise is resolved.
// Line 12: We return a simple string 'Hello world'.
// Line 15: We call then on the result of the then call on line 10. The promise here is an auto-resolved promise, and it will pass 'Hello world' in the data. When you are in a then callback, if you return anything but a promise, an auto-resolved promise is created, and this promise will be the result of the then call.
// Line 16: We print data3 and this is the 'Hello world' returned at line 12.
// Line 17: End of line 3 (yes, this is a very long line!). At this point, you can add a then call if you want. then always returns a promise. And if you decide to return nothing (like at line 16, we don't return anything), then returns an auto-resolved pxromise with no data (if you try to get any data, you'll get undefined).




job1()
    .then(function (data1) {
        console.log('data1', data1);
        return job2();
    })

    .then(function (data2) {
        console.log('data2', data2);
        return 'Hello world';
    })

    .then(function (data3) {
        console.log('data3', data3);
    });

function job1() {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            resolve('result of job 1');
        }, 1000);
    });
}

function job2() {
    return new Promise(function (resolve, reject) {
        setTimeout(function () {
            resolve('result of job 2'   );
        }, 1000);
    });
}




// module.exports = job;